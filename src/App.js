import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import PostList from "./components/PostList/PostList";
import {Provider} from 'react-redux';
import store from './stores/store';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo"/>
                        <h1 className="App-title">Welcome to React</h1>
                    </header>
                    <p className="App-intro">
                        hi hello world 1231231.
                    </p>
                    <PostList/>
                </div>
            </Provider>
        );
    }
}

export default App;
