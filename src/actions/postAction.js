import {FETCH_POST} from './types';
import PostService from './../services/post-service';

// const postService = new PostService();

export const fetchPost = () => (dispatch) => {

    console.log("calling fetch");
    // fetch('https://jsonplaceholder.typicode.com/posts')
    //     .then(res => res.json())
    //     .then(body => {
    //         dispatch({
    //             type: FETCH_POST,
    //             payload: body
    //         })
    //     });

    // axios.get('https://jsonplaceholder.typicode.com/posts')
    //     .then(body => {
    //         console.log('have body:', body);
    //         dispatch({
    //             type: FETCH_POST,
    //             payload: body.data
    //         });
    //     });

    PostService.getPosts()
        .then(body => {
            console.log('have body:', body);
            dispatch({
                type: FETCH_POST,
                payload: body.data
            });
        });

};