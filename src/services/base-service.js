import axios from 'axios';

const domain = 'https://jsonplaceholder.typicode.com';

export default class BaseService {

    http;

    constructor(apiPath) {
        this.http = axios.create({
            baseURL: domain.concat('/', apiPath),
            transformRequest: [this.transformRequest]
        })
    }

    transformRequest(data, header) {
        console.log('header is ', header);
        console.log('data is ', data);
        header.authentication = 'bearer 123';
        return JSON.stringify(data);
    }


}