
import BaseService from './base-service'

class PostService extends BaseService{

    constructor() {
        super('posts')
    }

    async getPosts() {
        return await this.http.get();
    }
}

export default new PostService();