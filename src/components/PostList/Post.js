import React, {Component} from "react";

class Post extends React.Component {
    render() {
        return (
            <div>
                <label>Title: {this.props.title}</label>
                <p>
                    {this.props.body}
                </p>
            </div>
        )
    }
}

export default Post;