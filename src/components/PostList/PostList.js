import Post from './Post';
import React, {Component} from "react";
import {connect} from 'react-redux';
import {fetchPost} from '../../actions/postAction';

class PostList extends React.Component {

    componentWillMount() {
        this.props.fetchPost();
    }

    render() {

        let posts = this.props.posts.map((item) => {
            return <Post key={item.id.toString()} title={item.title} body={item.body}></Post>

        });

        return (
            <div>
                {posts}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    posts: state.posts.items
});

export default connect(mapStateToProps, {fetchPost})(PostList);